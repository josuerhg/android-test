package com.jrhg.android1_josuehernandez;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import java.util.Collections;
import java.util.Set;

//Made by Josué HG for Alto Mobile

public class LoginActivity extends AppCompatActivity {
    //Callback manager used for FB authentication
    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        LoginButton loginButton;

        //Finds the login button we created in the XML file, and assigns it to the variable we created
        loginButton = findViewById(R.id.login_button);
        //Read permissions
        loginButton.setReadPermissions("email");

        callbackManager = CallbackManager.Factory.create();

        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                if (loginResult.getAccessToken() != null) {
                    Set<String> deniedPermissions = loginResult.getRecentlyDeniedPermissions();

                    if (deniedPermissions.contains("email")) {
                        //If the user denies email permissions, he/she won't be redirected to the Main Activity
                        LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Collections.singletonList("email"));
                        //...And a toast will be shown
                        Toast.makeText(getApplicationContext(), R.string.email_denied, Toast.LENGTH_SHORT).show();
                    }
                    else {
                        //If the user logs in successfully (and grants email permission), he/she'll be redirected to the Main Activity
                        toMainActivity();
                    }
                }

            }

            @Override
            public void onCancel() {
                //If the login is cancelled, a Toast will be shown
                //"cancel_login" is a custom string, which specifies what text will the Toast have in it
                Toast.makeText(getApplicationContext(), R.string.cancel_login, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException exception) {
                //See above, familia
                Toast.makeText(getApplicationContext(), R.string.login_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void toMainActivity() {
        //An intent allows us to "prepare" a call for another activity
        Intent intent = new Intent(this, MainActivity.class);
        //These flags are used to ensure the current activity is the only activity in execution
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        //The activity of our intent (Main Activity, in this case) is called to start
        startActivity(intent);
    }

    //Used for Facebook authentication
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }
}
