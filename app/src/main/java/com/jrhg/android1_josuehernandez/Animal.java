package com.jrhg.android1_josuehernandez;

public class Animal {
    private int life, id;
    private String name, pictureURL;

    //Constructor
    public Animal(int myLife, int myId, String myName, String myPictureURL) {
        life = myLife;
        id = myId;
        name = myName;
        pictureURL = myPictureURL;
    }
    //Default constructor
    public Animal() {
        life = 0;
        id = 999;
        name = "Default";
        pictureURL = "";
    }

    //Getters
    public int getId() {
        return id;
    }
    public int getLife() {
        return life;
    }
    public String getName() {
        return name;
    }
    public String getPictureURL() {
        return pictureURL;
    }

    //Setters
    public void setId(int myId) {
        id = myId;
    }
    public void setName(String myName) {
        name = myName;
    }
    public void setLife(int myLife) {
        life = myLife;
    }
    public void setPictureURL(String myPictureURL) {
        pictureURL = myPictureURL;
    }
}
