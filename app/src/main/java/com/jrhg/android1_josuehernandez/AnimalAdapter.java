package com.jrhg.android1_josuehernandez;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

public class AnimalAdapter extends ArrayAdapter<Animal> {

    private Context mContext;
    private ArrayList<Animal> animalsList;

    //The constructor takes a context and an ArrayList<Animal> as arguments (of course)
    AnimalAdapter(@NonNull Context context, ArrayList<Animal> list) {
        super(context, 0 , list);
        mContext = context;
        animalsList = list;
    }

    //Now, here comes the tricky part. We have to create a method that returns a View, which is
    //going to do the important part of an adapter: converting a List/Array into a ListView
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.animal_item,parent,false);

        //We get the animal in the current position
        Animal currentAnimal = animalsList.get(position);

        //We declare our ImageView and specify what ImageView we're using (animal_picture, in this case)
        ImageView image = listItem.findViewById(R.id.animal_picture);
        //Now, take a look at this. Normally, handling media hosted on the internet in Android would be a pain in the ass,
        //but thankfully, there's a beautiful API called Glide, which makes our lives much easier
        //Thanks to Glide, we can do something like this to assign an image to an ImageView
        Glide.with(mContext).load(currentAnimal.getPictureURL()) //sets the image to whatever Glide finds in the URL
                .apply(new RequestOptions()
                        .placeholder(R.drawable.ic_placeholder) //allows us to choose a placeholder for the image
                        .centerCrop() //self-explanatory
                        .diskCacheStrategy(DiskCacheStrategy.NONE) //I chose 'NONE' as my DiskCacheStrategy
                        //because I don't want to save anything in the app's cache (for now, since it's a test app)
                        .skipMemoryCache(false) //if we set this to true, images might get deleted from cache while scrolling
                        .dontAnimate()) //self-explanatory
                .into(image); //choosing the ImageView that will have the picture

        //I could've finished Step 2 on Wednesday if I knew about Glide beforehand...

        //We choose the TextView from our animal_item and set its text to the current animal's name
        TextView name = listItem.findViewById(R.id.animal_name);
        name.setText(currentAnimal.getName());

        return listItem;
    }
}
