package com.jrhg.android1_josuehernandez;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    //Variable declarations
    ListView animalListView;
    ArrayList<Animal> animalsAL;
    AnimalAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //If the user hasn't logged in and tries to reach the MainActivity, he'll be redirected
        //to the login activity
        if(AccessToken.getCurrentAccessToken() == null)
        {
            toLoginActivity();
        }

        //Declaring HTTP client and specifying the JSON's URL
        OkHttpClient animalsClient = new OkHttpClient();
        String url = "https://flavioruben.herokuapp.com/data.json";
        Request request = new Request.Builder().url(url).build();

        //Making a request call, in order to get the JSON object that's going to provide us
        //with our animals and their data
        animalsClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                //If the request fails, the user will be notified
                Toast.makeText(getApplicationContext(), R.string.request_fail, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                if(response.isSuccessful()) {
                    //The JSON obtained from the HTTP request is converted into a good old String
                    assert response.body() != null;
                    final String animalsJson = response.body().string();

                    //We declare a Gson, which is going to help us do some pretty nice converting magic
                    //Gson is a library from Google btw, which makes handling JSONs a breeze
                    Gson gson = new Gson();

                    //We have to declare a Type for the Gson to know how to convert the JSON we've got
                    Type animalListType = new TypeToken<ArrayList<Animal>>(){}.getType();

                    //Using Google's Gson magic, we're able to store the content of the request's JSON in an ArrayList
                    animalsAL = gson.fromJson(animalsJson, animalListType);

                    //We CAN'T modify the app's UI from this function, so we'll run it on an UI thread
                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //Toast.makeText(getApplicationContext(), animalArray.get(0).name, Toast.LENGTH_SHORT).show();

                            //We initialize our List View and adapt it using our custom AnimalAdapter
                            animalListView = findViewById(R.id.animalListView);
                            adapter = new AnimalAdapter(getApplicationContext(), animalsAL);
                            animalListView.setAdapter(adapter);

                            //When the user clicks on an item, it will display a toast with the clicked animal's life attribute
                            animalListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    Animal animalClick = animalsAL.get(position);
                                    Toast.makeText(parent.getContext(), "Life = " + String.valueOf(animalClick.getLife()), Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    });

                }
            }
        });
    }



    //======================================
    //          ADDITIONAL FUNCTIONS
    //======================================

    //Redirects the user to the Login activity
    private void toLoginActivity() {
        //An intent allows us to "prepare" a call for another activity
        Intent intent = new Intent(this, LoginActivity.class);
        //These flags are used to ensure the current activity is the only activity in execution
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        //The activity of our intent (Login Activity, in this case) is called to start
        startActivity(intent);
    }

    //Method to be called when the user click on the "Logout" button
    public void logout(View view) {
        LoginManager.getInstance().logOut();
        //When the user logs out, he'll be redirected to the Login activity
        toLoginActivity();
    }
}